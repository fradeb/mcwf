\documentclass{beamer}
\usepackage{braket}
\usepackage{graphicx, array}
\usepackage[skip=10pt plus1pt, indent=0pt]{parskip}

\title{The renormalized hamiltonian approach\\{\small Seminar in Condensed Matter Physics}}
\date{}

\begin{document}

\frame{\titlepage}

\begin{frame}
    \begin{itemize}
        \item Originally devised by Hamann\&Overhauser (1966), revisited by Yarlagadda\&Giuliani (1989, 1994), provides an explicit construction of the Landau quasiparticles.
        \item Separate the Hilbert space in fast and slow degrees of freedom, using an arbitrarly small cutoff $\Lambda$:
        \begin{align}
            \hat{a}_{\vec{k}\sigma}=
            \begin{cases}
                \hat{\alpha}_{\vec{k}\sigma} & |k-k_F| < \Lambda\\
                \hat{\beta}_{\vec{k}\sigma}  & |k-k_F| > \Lambda\\
            \end{cases}
        \end{align}
        \item Goal: derive an effective hamiltonian for the $\alpha$ operators (\textit{test particle operators}).
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item The result is expressed as the sum of three terms:
        \begin{align}
            \hat{H} = \hat{H}_0 + \hat{H}_M + \hat{H}_I
        \end{align}
        \item The first term
        \begin{align}
            \hat{H}_0 = &\sum_{\vec{k}\sigma}\tilde{\epsilon}_{\vec{k}}\hat{\alpha}^{\dagger}_{\vec{k}\sigma}\hat{\alpha}_{\vec{k}\sigma}\\&+\frac{1}{2L^d}\sum_{\vec{q}\neq 0}v_q\sum_{\vec{k}_1\sigma_1\vec{k}_2\sigma_2}\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}^\dagger_{\vec{k}_2-\vec{q}\sigma_2}\hat{\alpha}_{\vec{k}_2\sigma_2}\hat{\alpha}_{\vec{k}_1\sigma_1}
        \end{align}
        where all the wavevectors lie withing a shell of thickness $\Lambda$ about $k_F$.
        \item We shall regard the second term as a kind of \textit{medium}:
        \begin{align}
            \hat{H}_M = &\sum_{\vec{k}\sigma}\tilde{\epsilon}_{\vec{k}}\hat{\beta}^{\dagger}_{\vec{k}\sigma}\hat{\beta}_{\vec{k}\sigma}\\&+\frac{1}{2L^d}\sum_{\vec{q}\neq 0}v_q\sum_{\vec{k}_1\sigma_1\vec{k}_2\sigma_2}\hat{\beta}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\beta}^\dagger_{\vec{k}_2-\vec{q}\sigma_2}\hat{\beta}_{\vec{k}_2\sigma_2}\hat{\beta}_{\vec{k}_1\sigma_1}
        \end{align}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Key observation: $\hat{H}_M$ reduces to the full electron liquid hamiltonian in the limit $\Lambda \rightarrow 0$.
        \item The interaction term contains $2\times2\times2\times2-2=14$ different terms, but guided by physical intuition we only keep the ones that separetely conserve the number of test particles and medium particles: we keep $\alpha^\dagger\beta^\dagger\beta\alpha$, but we exclude $\alpha^\dagger\alpha^\dagger\beta\beta$, neglecting terms that typically create high-energy virtual excitations.
        \begin{align}
            \hat{H}_I = &\frac{1}{L^d}\sum_{\vec{q}\neq 0}v_q\sum_{\vec{k}_1\sigma_1\vec{k}_2\sigma_2}\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\beta}^\dagger_{\vec{k}_2-\vec{q}\sigma_2}\hat{\beta}_{\vec{k}_2\sigma_2}\hat{\alpha}_{\vec{k}_1\sigma_1}\\-&\frac{1}{L^d}\sum_{\vec{q}\neq 0}v_{\vec{k}_2-\vec{k}_1-\vec{q}}\sum_{\vec{k}_1\sigma_1\vec{k}_2\sigma_2}\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\beta}^\dagger_{\vec{k}_2-\vec{q}\sigma_2}\hat{\beta}_{\vec{k}_2\sigma_1}\hat{\alpha}_{\vec{k}_1\sigma_2}
        \end{align}
        \item Recall the form of the density and spin fluctuaction operator $\hat{n}_{\vec{q}}=\sum_{\vec{k}\sigma}\hat{\beta}^\dagger_{\vec{k}-\vec{q}\sigma}\hat{\beta}_{\vec{k}\sigma}$ and $\hat{\vec{S}}_{\vec{q}} = \sum_{\vec{k}\sigma_1\sigma_2}\hat{\beta}^\dagger_{\vec{k}-\vec{q}\sigma_1}\vec{\sigma}_{\sigma_1\sigma_2}\hat{\beta}_{\vec{k}\sigma_2}$.
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item The first term can be expressed exactly as an interaction between \textit{density fluctuaction} of two different types of particles.
        \item The second term represents an exchange process in which a particle from the medium replaces a test particle and vice versa. Notice that the two particles can have opposite spins and in this case the second term transfers spin angular momentum between the test particles and the medium.
        \item So we assume that the interaction between the test particles and the medium can be expressed as an interaction between density and spin fluctuactions of the test particles and the medium.
    \end{itemize}
\end{frame}

\begin{frame}
    \includegraphics[width=\textwidth]{termini.jpg}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item $v_n(q)$ and $v_s(q)$ must approximately include both the exchange effect (the second term) and all the high energy processes exlcuded from $\hat{H}_I$:
    \begin{align}
        \hat{H}_I \approx &\frac{1}{L^d}\sum_{\vec{q}\neq 0}v_n(q)\sum_{\vec{k}\sigma}\hat{n}_{-\vec{q}}\hat{\alpha}^{\dagger}_{\vec{k}-\vec{q}\sigma}\hat{\alpha}_{\vec{k}\sigma}\\
        &+\frac{1}{L^d}\sum_{\vec{q}\neq 0}v_s(q)\sum_{\vec{k}\sigma_1\sigma_2}\hat{\vec{S}}_{-\vec{q}}\cdot\left(\hat{\alpha}^{\dagger}_{\vec{k}-\vec{q}\sigma_1}\vec{\sigma}_{\sigma_1\sigma_2}\hat{\alpha}_{\vec{k}\sigma_2}\right)
    \end{align}
        \item The interaction hamiltonian contains terms quadratic in the test particle operators. We seek and find a unitary transformation that eliminates this interaction up to terms quartic in the test particle operators:
        \begin{align}
            \hat{\bar{H}} &= e^{i\hat{F}}\hat{H}e^{-i\hat{F}}\\
            \hat{\bar{H}} &\approx \hat{H}_0 + \hat{H}_M + \hat{H}_I + i[\hat{F}, \hat{H}_0+\hat{H}_M]+ i[\hat{F}, \hat{H}_I]\\-&\frac{1}{2}[\hat{F}, [\hat{F}, \hat{H}_0+\hat{H}_M]]
        \end{align}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item We thus must choose $\hat{F}$ as the solution of the operator equation:
        \begin{align}
            \label{opequation}
            i[\hat{F}, \hat{H}_0+\hat{H}_M] = -\hat{H}_I
        \end{align}
        \item We are left with the second-order hamiltonian
        \begin{align}
            \hat{\bar{H}} \approx \hat{H}_0+\hat{H}_M+\frac{i}{2}[\hat{F}, \hat{H}_I]
        \end{align}
        \item To complete the elimination of the fast degrees of freedom we average $\hat{\bar{H}}$ in the ground-state of the medium (or in the termal equilibrium ensamble) and discard the constant term:
        \begin{align}
            \hat{H}_{qp} = \hat{H}_0 + \frac{i}{2}\braket{0|[\hat{F}, \hat{H}_I]|0}
        \end{align}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item We seek a solution in the form of a one-particle operator in the space of test particles:
            \begin{align}
                \hat{F} = \frac{1}{L^d}\sum_{\vec{q}\neq 0}\sum_{\vec{k}\sigma\sigma'}\hat{\mathcal{F}}_{\sigma\sigma'}(\vec{k}, \vec{q})\hat{\alpha}^{\dagger}_{\vec{k}-\vec{q}\sigma}\hat{\alpha}_{\vec{k}\sigma'}
            \end{align}
        \item Inserting this guess in equation $\ref{opequation}$, we calculate $\braket{m|\hat{\mathcal{F}}_{\sigma\sigma'}(\vec{k},\vec{q})|n}$ and we insert these matrix elements in
        \begin{align}
            \hat{H}_{qp} = \hat{H}_0+\frac{i}{2}\sum_{n}\{\braket{0|\hat{F}|n}\braket{n|\hat{H}_I|0}-\braket{0|\hat{H}_I|n}\braket{n|\hat{F}|0}\}
        \end{align}
    \end{itemize} 
\end{frame}

\begin{frame}
    \begin{itemize}
        \item We obtain (in a paramagnetic system were we can use the translational and spin rotational invariance of the medium):
        {\small\begin{align*}
            &\hat{H}_{qp} = \sum_{\vec{k}\sigma}\tilde{\epsilon}_{\vec{k}}\hat{\alpha}^{\dagger}_{\vec{k}\sigma}\hat{\alpha}_{\vec{k}\sigma}\\&+\frac{1}{2L^d}\sum_{\vec{q}\neq 0}v_q\sum_{\vec{k}_1\sigma_1\vec{k}_2\sigma_2}\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}^\dagger_{\vec{k}_2-\vec{q}\sigma_2}\hat{\alpha}_{\vec{k}_2\sigma_2}\hat{\alpha}_{\vec{k}_1\sigma_1}
            \\&+\frac{1}{2L^d}\sum_{\vec{q}\neq 0}v^2_n(q)\sum_{\vec{k}_1\vec{k}_2}M_n(\vec{k}_1, \vec{k}_2, \vec{q})
            \\&\left(\sum_{\sigma_1}\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}_{\vec{k}_1\sigma_1}\right)\left(\sum_{\sigma_2}\hat{\alpha}^\dagger_{\vec{k}_2-\vec{q}\sigma_2}\hat{\alpha}_{\vec{k}_2\sigma_2}\right)
            \\&+\frac{1}{2L^d}\sum_{\vec{q}\neq 0}v^2_s(q)\sum_{\vec{k}_1\vec{k}_2}M_s(\vec{k}_1, \vec{k}_2, \vec{q})
            \\&\left(\sum_{\sigma_1\sigma_2}\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\vec{\sigma}_{\sigma_1\sigma_2}\hat{\alpha}_{\vec{k}_1\sigma_2}\right)\cdot\left(\sum_{\tau_1\tau_2}\hat{\alpha}^\dagger_{\vec{k}_2-\vec{q}\tau_1}\vec{\sigma}_{\tau_1\tau_2}\hat{\alpha}_{\vec{k}_2\tau_2}\right)
        \end{align*}}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item The last two terms describe an effective interaction between the test particles that is mediated by density and spin fluctuactions respectively:
        {\tiny\begin{align*}
            M_n\left(\vec{k}_1, \vec{k}_2, \vec{q}\right) \equiv \frac{1}{L^d}\sum_n\left\{\frac{|\braket{n|\hat{n}_{\vec{q}}|0}|^2}{\tilde{\epsilon}_{\vec{k}_2}-\tilde{\epsilon}_{\vec{k}_2-\vec{q}}+E_0-E_n}-\frac{|\braket{n|\hat{n}_{\vec{q}}|0}|^2}{\tilde{\epsilon}_{\vec{k}_1}-\tilde{\epsilon}_{\vec{k}_1+\vec{q}}+E_n-E_0}\right\}\\
            M_s\left(\vec{k}_1, \vec{k}_2, \vec{q}\right) \equiv \frac{1}{L^d}\sum_n\left\{\frac{|\braket{n|\hat{S}_{z,\vec{q}}|0}|^2}{\tilde{\epsilon}_{\vec{k}_2}-\tilde{\epsilon}_{\vec{k}_2-\vec{q}}+E_0-E_n}-\frac{|\braket{n|\hat{S}_{z,\vec{q}}|0}|^2}{\tilde{\epsilon}_{\vec{k}_1}-\tilde{\epsilon}_{\vec{k}_1+\vec{q}}+E_n-E_0}\right\}
        \end{align*}}
        \item Interestingly, these terms also include a \textit{self-interaction} of each test particle with itself. Physically, a test particle polarizes the medium which in turns acts back on the test particle. Mathematically, we have $\hat{\alpha}^\dagger\hat{\alpha}\hat{\alpha}^\dagger\hat{\alpha}$ and not the normal orderdered $\hat{\alpha}^\dagger\hat{\alpha}^\dagger\hat{\alpha}\hat{\alpha}$ (recall that a two body operator free of self-interaction of the form $\sum_{i\neq j}\hat{V}_{ij}$ gives a normal order in second quantization).
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item In order to exctract the self-interaction:
        \begin{align}
            \hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}_{\vec{k}_1\sigma_2}\hat{\alpha}^\dagger_{\vec{k}_2-\vec{q}\tau_1}\hat{\alpha}_{\vec{k}_2\tau_2} = \hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}^\dagger_{\vec{k}_2-\vec{q}\tau_1}\hat{\alpha}_{\vec{k}_2\tau_2}\hat{\alpha}_{\vec{k}_1\sigma_2}
            \\+ \hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}_{\vec{k}_2\tau_2}\delta_{\vec{k}_2, \vec{k_1}+\vec{q}}\delta_{\sigma_2, \tau_1}
        \end{align}
        \item Thus the \textit{one-body part} of $\hat{H}_{qp}$ is then
        \begin{align}
            &\hat{H}_{qp, 1} = \sum_{\vec{k}\sigma}\Bigg\{\tilde{\epsilon}_{\vec{k}\sigma}
            +\frac{1}{L^d}\sum_{\vec{q}\neq 0}v_n^2(q)\frac{1}{L^d}\sum_n\frac{|\braket{n|\hat{n}_{\vec{q}}|0}|^2}{\tilde{\epsilon}_{\vec{k}}-\tilde{\epsilon}_{\vec{k}-\vec{q}}+E_0-E_n}\\
            &+3\frac{1}{L^d}\sum_{\vec{q}\neq 0}v_s^2(q)\frac{1}{L^d}\sum_n\frac{|\braket{n|\hat{S}_{z,\vec{q}}|0}|^2}{\tilde{\epsilon}_{\vec{k}}-\tilde{\epsilon}_{\vec{k}-\vec{q}}+E_0-E_n}\Bigg\}\hat{\alpha}^\dagger_{\vec{k}\sigma}\hat{\alpha}_{\vec{k}\sigma}
        \end{align}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item The sum over the exact eigenstates of the medium can be written as
        \begin{align}
        \frac{1}{L^d}\sum_n\frac{|\braket{n|\hat{n}_{\vec{q}}|0}|^2}{\tilde{\epsilon}_{\vec{k}}-\tilde{\epsilon}_{\vec{k}-\vec{q}}+E_0-E_n}
        = -\frac{1}{\pi}\mathcal{P}\int_0^{\infty}d\omega \frac{\Im \chi_{nn}(q, \omega)}{\frac{\tilde{\epsilon}_{\vec{k}}-\tilde{\epsilon}_{\vec{k}-\vec{q}}}{\hbar}-\omega}
        \end{align}
        where $\chi_{nn}(q, \omega)$ is the density-density response function of the medium, which coincides for $\Lambda\rightarrow 0$ with the density-density response function of the electon liquid itself.
        \item This shift in the single particle energy is often referred as the \textit{coulomb hole} term.
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
    \item The two-body part can be written as
    {\small\begin{align}
       \hat{H}_{qp, 2} = \frac{1}{2L^d}\sum_{\vec{q}\neq 0} \sum_{\vec{k}_1\vec{k}_2}\sum_{\sigma_1\sigma_2, \tau_1, \tau_2}W(\vec{k_2}, \vec{q})_{\sigma_1\sigma_2, \tau_1\tau_2}\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}^{\dagger}_{\vec{k}_2-\vec{q}\tau_1}\hat{\alpha}_{\vec{k_2}\tau_2}\hat{\alpha}_{\vec{k}_1\sigma_2}
    \end{align}}
    where $W(\vec{k}, \vec{q})$ is a Kukkonen-Overhauser-like interaction potential of the form:
    \begin{align}
        W(\vec{k}, \vec{q})_{\sigma_1\sigma_2, \tau_1\tau_2} = W_+(\vec{k}, \vec{q})\delta_{\sigma_1\sigma_2}\delta_{\tau_1\tau_2}+W_-(\vec{k}, \vec{q})\vec{\sigma}_{\sigma_1\sigma_2}\cdot\vec{\sigma}_{\tau_1\tau_2}
    \end{align}
    which is clearly spin rotationally invariant. The coefficients are
    \begin{align}
        W_+(\vec{k}, \vec{q}) &= v_q + v_n^2(q)\Re\chi_{nn}\left(q, \frac{\epsilon_{\vec{k}}-\epsilon_{\vec{k}-\vec{q}}}{\hbar}\right)\\
        W_-(\vec{k}, \vec{q}) &= v_s^2(q)\Re\chi_{S_zS_z}\left(q, \frac{\epsilon_{\vec{k}}-\epsilon_{\vec{k}-\vec{q}}}{\hbar}\right)
    \end{align}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Recall that in the limit $\Lambda = 0$ the quartic term $\hat{H}_{qp, 2}$ is small, because there are two $\sum \sum$ and thus it can be treated by first-order perturbation theory. Using Wick theorem:
        \begin{align}
            \braket{\Psi|\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}^{\dagger}_{\vec{k}_2-\vec{q}\tau_1}\hat{\alpha}_{\vec{k_2}\tau_2}\hat{\alpha}_{\vec{k}_1\sigma_2}|\Psi} =
            -\\\braket{\Psi|\hat{\alpha}^{\dagger}_{\vec{k}_1+\vec{q}\sigma_1}\hat{\alpha}_{\vec{k_2}\tau_2}|\Psi}\braket{\Psi|\hat{\alpha}^{\dagger}_{\vec{k}_2-\vec{q}\tau_1}\hat{\alpha}_{\vec{k}_1\sigma_2}|\Psi} =
            \\\delta_{\tau_2\sigma_1}\delta_{\tau_1\sigma_2}\delta_{\vec{k}_1, \vec{k}_2-\vec{q}}n^{(0)}_{\vec{k}_2}n^{(0)}_{\vec{k}_2-\vec{q}}
        \end{align}
        since $\vec{q}\neq 0$ we just have the exchange term. Thus we have a \textit{screened exchange} potential (screened because the Kukkon-Overhauser potential appears):
        \begin{align}
            \Sigma_{sx, \vec{k}} = -\int \frac{d\vec{q}}{(2\pi)^d}\left[W_+(\vec{k}, \vec{q})+3W_-(\vec{k}, \vec{q})\right]n^{(0)}_{\vec{k}-\vec{q}}
        \end{align}
        \item Thus the quasiparticle energy, relative to the chemical potential, is the sum of three contributions:
        \begin{align}
            \mathcal{E}_{\vec{k}}-\mu = \tilde{\epsilon}_{\vec{k}}+\Sigma_{ch, \vec{k}}+\Sigma_{sx, \vec{k}}
        \end{align}
    \end{itemize}
\end{frame}

\begin{frame}
   \begin{itemize}
    \item We obtained the quasiparticles spectrum of the hamiltonian $\hat{H}_{qp}$. In other words, the operators $\hat{\alpha}^\dagger_{\vec{k}\sigma}$ appearing in the renormalized hamiltonian creates a quasiparticle, adding an energy of $\mathcal{E}_{\vec{k}}$ to the renormalized system. Thus the corresponding operator meant to be applied to the original interacting ground state of $\hat{H}$ is therefore given by
    \begin{align}
        e^{-i\hat{F}}\hat{\alpha}_{\vec{k}\sigma}e^{i\hat{F}}
    \end{align}
    \item We know that $Z$ measures the strength of the quasiparticle peak in the spectral function. To compute it:
    \begin{align}
        G_{\sigma}(\vec{k}, t) &= -i\braket{\Psi_0|T\hat{\alpha}_{\vec{k}\sigma}(t)\hat{\alpha}^{\dagger}_{\vec{k}\sigma}|\Psi_0}\\
        &= -i\braket{\bar{\Psi}_0|T\hat{\bar{\alpha}}_{\vec{k}\sigma}(t)\hat{\bar{\alpha}}^{\dagger}_{\vec{k}\sigma}|\bar{\Psi}_0}
    \end{align}
    where $\ket{\Psi_0}$ is the ground state of the original hamiltonian $\hat{H}$ and $\ket{\bar{\Psi}_0}$ is the ground state of $\hat{\bar{H}}$.
   \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item This is convinient because $\ket{\bar{\Psi}}$ can be factorized in the ground state of the medium and the ground state for the quasiparticles and their evolution is also factorized and given respectively by the medium hamiltonian $\hat{H}_M$ and $\hat{H}_{qp}$. Thus we obtain:
        \begin{align}
            \label{coherence}
            G_{\sigma}(\vec{k}, t) = \sum_{\vec{k}'\sigma'}\langle(e^{-i\hat{M}(t)})_{\vec{k}\sigma, \vec{k}'\sigma'}(e^{i\hat{M}})_{\vec{k}'\sigma', \vec{k}\sigma}\rangle_M G_{qp, \sigma'}(\vec{k}', t)
        \end{align}
        where
        \begin{align}
            \hat{M}_{\vec{k}\sigma, \vec{k}'\sigma'} &\equiv \frac{1}{L^d}\hat{\mathcal{F}}_{\sigma, \sigma'}(\vec{k}, \vec{k}-\vec{k}')\\
            G_{qp, \sigma}(\vec{k}, t) &= -i\braket{\bar{\Psi}_{0, qp}|T\hat{\alpha}_{\vec{k}\sigma}(t)\hat{\alpha}^{\dagger}_{\vec{k}\sigma}|\bar{\Psi}_{0, qp}}\\
            &= -ie^{-\frac{i}{\hbar}(\mathcal{E}_{\vec{k}\sigma}-\mu)t}\left[\Theta(t)(1-n^{(0)}_{\vec{k}\sigma})-\Theta(-t)n^{(0)}_{\vec{k}\sigma}\right]
        \end{align}
        is the usual free-fermion Green's function generated by the hamiltonian $\hat{H}_{qp}$ with the quartic term appropriately neglected.
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Equation \ref{coherence} reveals that the Green's function retains a long time coherence that is characteristic of the Landau-Fermi liquid state:
        \begin{align}
            \langle(e^{-i\hat{M}(t)})_{\vec{k}\sigma, \vec{k}'\sigma'}(e^{i\hat{M}})_{\vec{k}'\sigma', \vec{k}\sigma}\rangle_M \rightarrow |\langle(e^{-i\hat{M}})_{\vec{k}\sigma, \vec{k}\sigma}\rangle_M|^2\delta_{\vec{k},\vec{k}'}\delta_{\sigma, \sigma'}
        \end{align}
        because the average of the product reduces to the product of the averages and due to the translational and rotational invariance of the medium, only the terms with $\vec{k}=\vec{k}'$ and $\sigma = \sigma'$ survive the averaging.
        \item Defining
        \begin{align}
            Z_\sigma \equiv |\langle(e^{-i\hat{M}})_{k_F\sigma, k_F\sigma}\rangle_M|^2\\
            G_{\sigma}(\vec{k}, t) \rightarrow Z_{\sigma}G_{qp, \sigma}(\vec{k}, t)
        \end{align}
        shows that the Green's function retains a particle-like long-term coherence, only with a reduced strength $Z_{\sigma}$. Fourier transformation yields a quasiparticle peak in the spectral function at $\omega = \mathcal{E}_{\vec{k}\sigma}$ with strength $Z_{\sigma}$.
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Having neglected quartic terms in the $H_{qp}$ hamiltonian we don't find any information about the behaviour of $\Gamma_{\vec{k}\sigma} = Z_{\vec{k}\sigma}|\Im \Sigma_{\sigma}^{ret}(\vec{k}, \mathcal{E}_{\vec{k\sigma}})|$, that results zero in this approximation: the quasiparticles mantain long-term coherence. This makes sense because we are only retaining terms in first order in $\Lambda \approx |k-k_F|$ and we know that $\Gamma_{\vec{k}\sigma} \approx (k-k_F)^{2}$.
        \item Using the expansion of $Z_{\sigma}$ to second order in the medium operator $\hat{M}$ (first order is zero)
            \begin{align}
                Z_{\sigma} = 1 + \sum_{\vec{k}'\sigma'}\sum_n \braket{0|\hat{M}_{\vec{k}\sigma, \vec{k}'\sigma'}|n}\braket{n|\hat{M}_{\vec{k}'\sigma', \vec{k}\sigma} |0}
            \end{align}
            one can show that
            \begin{align}
                Z_{\sigma} = 1 + \left.\frac{1}{\hbar}\frac{\partial \Re \Sigma_\sigma(k_F, \omega)}{\partial\omega}\right\vert_{\hbar\omega = \mu}
            \end{align}
            consistently with the macroscopic theory.
    \end{itemize}
\end{frame}

\end{document}
\documentclass{beamer}
\usepackage{subfig}
\usepackage{braket}
\usepackage{graphicx, array}
\usepackage[skip=10pt plus1pt, indent=0pt]{parskip}
\usepackage{graphicx, array}

\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}


\renewcommand{\vec}[1]{\boldsymbol{#1}}
\setbeamertemplate{section in toc}[sections numbered]



\usepackage{yfonts}
\title{Monte Carlo wave-function method in quantum optics\\{\small Seminars in Condensed Matter Physics}}
\author{Francesco Debortoli}
\date{Spring 2023}

\begin{document}

\frame{\titlepage}

\begin{frame}
    \begin{figure}[H]
        \includegraphics[width=\textwidth]{figures/title.png}
    \end{figure}
\end{frame}

\begin{frame}
\tableofcontents
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \section{Introduction}
    The problem of dissipation plays a central role in quantum optics (e.g. spontaneous emission).

    Usually the dissipative coupling between a small system and a large reservoir can be treated by a master-equation approach. If we denote $\rho_S = \text{Tr}_\text{res}(\rho)$ the density matrix of the system, this equation can be written as
    \begin{align}
        \dot{\rho}_S = \frac{i}{\hbar}[\rho_S, H_S]+\mathcal{L}_\text{relax}(\rho_S)
    \end{align}
    $\mathcal{L}_\text{relax}$ is the relaxation superoperator, acting on the density operator $\rho_S$. It is assumed here to be local in time, which means that $\dot{\rho}_S$ depends only on $\rho_S(t)$ at the same time (Markov approximation). 
\end{frame}

\begin{frame}
    We present another approach to the problem based on the evolution of a Monte Carlo wave-function (MCWF) of the small system, which consists of two elements: 1) evolution with a non-Hermitian Hamiltonian and 2) randomly decided quantum jumps. 

    This approach, that we will prove to be equivalent to the master-equation treatment, is interesting for two reasons:
    \begin{itemize}
        \item if the relevant Hilbert space of the system has a dimension $N\gg 1$, the number of variables in the MCWF treatment ($\approx N$) is much smaller than the one required for the calculations with density matrices ($\approx N^2$);
        \item new physical insight may be gained.
    \end{itemize}
    
\end{frame}

\begin{frame}
    \frametitle{Quantum measurements in the POVM formalism}
    \section{Quantum measurements in the POVM formalism}
    Consider a set $\{E_j\}_{j = 1, \dots D}$ of positive self-adjoint operators, with $\sum_j E_j = 1$. We can compute the probability of the outcome $j$ as $p_j = \text{Tr}(E_j\rho_S)$.

    Why? Consider the system $S$ coupled with an ancillary system $A$ initialized in the state $\ket{\phi}_A$ where we can perform projective measurements. The probability of measuring $j$ is
    \begin{align}
        p_j = \vphantom{\ket{j}}_A\bra{j}\text{Tr}_S\left[U (\rho_S \otimes \ket{\phi}_{AA}\bra{\phi})U^\dagger \right]\ket{j}_A
            = \text{Tr}_S(M_j^\dagger M_j \rho_S)
    \end{align}
    where
    \begin{align}
        M_j = \vphantom{\ket{j}}_A \bra{j} U \ket{\phi}_A\hspace{1cm}E_j = M_j ^\dagger M_j
    \end{align}
    So given a POVM, the system after the measurement will collapse to
    \begin{align}
        \rho_S^{(j)} = \frac{M_j \rho_S M_j^\dagger}{\text{Tr}(\rho_S E_j)}
    \end{align}
    where $M_j = U_j \sqrt{E_j}$: the collapse is not fixed by the POVM!
\end{frame}

\begin{frame}
    \frametitle{General presentation of the method}
    \section{General presentation of the method}
    \textbf{The Lindblad form}

    The class of relaxation operator that we consider is the following:
    \begin{align}
        \mathcal{L}(\rho_S) = -\frac{1}{2}\sum_m (C_m^\dagger C_m \rho_S + \rho_S C_m^\dagger C_m) + \sum_m C_m \rho_S C_m^\dagger
    \end{align}
    Here we indicate the expression for the case of spontaneous emission by a two-level system or by a harmonic oscillator, where there is just a single operator $C_1 = \sqrt{\Gamma} \sigma^-$:
    \begin{align}
        \mathcal{L}(\rho_S) = - \frac{\Gamma}{2}(\sigma^+\sigma^-\rho_S+\rho_S\sigma^+\sigma^-)+\Gamma \sigma^-\rho_S \sigma^+
    \end{align}
\end{frame}

\begin{frame}
    \textbf{MCWF procedure}
    
    Consider at time $t$ that the system is in a state with the normalized wave function $\ket{\phi(t)}$. To get the wave-function at time $t + \delta t$:
    \begin{enumerate}
        \item Evolve $\ket{\phi(t)}$ with the non-Hermitian Hamiltonian:
        \begin{align}
            H &= H_S - \frac{i\hbar}{2}\sum_m C^\dagger_m C_m\\
            \label{evolution}
            \ket{\phi^{(1)}(t+\delta t)} &= \left(1- \frac{iH\delta t}{\hbar}\right)\ket{\phi(t)}
        \end{align}
        This new wave-function is not normalized:
        \begin{align}
            \label{normalization}
            \braket{\phi^{(1)}(t+\delta t)|\phi^{(1)}(t+\delta t)} = 1 - \delta p
        \end{align}
        where $\delta p$ reads as
        \begin{align}
            \delta p &= \delta t \frac{i}{\hbar} \braket{\phi(t)|H-H^\dagger|\phi(t)} = \sum_m \delta p_m \\\delta p_m &= \delta t \braket{\phi(t)|C_m^\dagger C_m |\phi(t)} \geq 0
        \end{align}
    \end{enumerate}
\end{frame}

\begin{frame}
    \begin{enumerate}
    \setcounter{enumi}{1}
        \item The second step of the evolution consists in a possible quantum jump. For particular examples we will see that this quantum jump can correspond to the projection of the wave-function associated with a \textit{gedanken} measurement process. We generate a random number $\epsilon \in [0, 1]$:
        \begin{itemize}
            \item if $\epsilon > \delta p$ (which occurs in most cases) no quantum jump occurs
            \begin{align}
                \ket{\phi(t+\delta t)} = \ket{\phi^{(1)}(t+\delta t)}/(1-\delta p)^{1/2}
            \end{align}
            \item if $\epsilon < \delta p$ a quantum jump occurs, and we choose the new normalized wave function among the different states:
            \begin{align}
                \ket{\phi(t+\delta t)} = C_m \ket{\phi(t)}/(\delta p_m/\delta t)^{1/2}\\ \text{ with probability } \Pi_m = \delta p_m / \delta p
            \end{align}
        \end{itemize}
    \end{enumerate}
\end{frame}

\begin{frame}
    \textbf{Equivalence with the Master Equation}

    We consider the quantity $\bar{\sigma}(t)$ obtained by averaging $\sigma(t) = \ket{\phi(t)}\bra{\phi(t)}$ over the various possible outcomes at time $t$ of the MCWF evolutions all starting in $\ket{\phi(0)}$.
    \begin{align}
        \nonumber
        \bar{\sigma}(t+\delta t) = (1-\delta p)\frac{\ket{\phi^{(1)}(t+\delta t)}}{(1-\delta p)^{1/2}}\frac{\bra{\phi^{(1)}(t+\delta t)}}{(1-\delta p)^{1/2}}\\
        +\delta p \sum_m \Pi_m \frac{C_m \ket{\phi(t)}}{(\delta p_m/\delta t)^{1/2}}\frac{\bra{\phi(t)}C_m^\dagger}{(\delta p_m/\delta t)^{1/2}}
    \end{align}
    which gives
    \begin{align}
        \frac{d \bar{\sigma}}{dt} = \frac{i}{\hbar}[\bar{\sigma}, H_S] + \mathcal{L}(\bar{\sigma})
    \end{align}
    If we assume that $\rho_S(0) = \ket{\phi(0)}\bra{\phi(0)}$, $\bar{\sigma}(t)$ and $\rho_S(t)$ coincide at any time. In the case where $\rho_S(0)$ does not correspond to a pure state $\rho(0) = \sum p_i \ket{\xi_i}\bra{\xi_i}$, then randomly choose the initial MCWF's among the $\ket{\xi_i}$ with the probability law $p_i$.
\end{frame}

\begin{frame}
    In the MCWF approach one calculates the one-time average value $\braket{A}(t) = \text{Tr}[\rho_S(t)A]$ as
    \begin{align}
        \braket{A}_{(n)}(t) = \frac{1}{n} \sum_{i=1}^n \braket{\phi^{(i)}(t)|A|\phi^{(i)}(t)}
    \end{align}

    What $\delta t$ should we choose?\\
    Maximum $\delta t$: we need $\delta p \ll 1$ \\
    Minimum $\delta t$: recall that the derivation of the master-equation makes use of coarse grain average of the real density operator evolution. The time step of this coarse-grain average has to be much larger than the correlation time $\tau_C$ of the reservoir (typically an optical period for the problem of spontaneous emission)
\end{frame}

\begin{frame}
    \frametitle{Physical interpretation of the procedure}
    Consider an harmonic oscillator/two levels atom that at $t=0$ is
    \begin{align}
        \ket{\phi(0)} = \alpha_0 \ket{0} + \beta_0 \ket{1}
    \end{align}
    The oscillator relaxes towards the ground state $\ket{0}$ throught the relaxation operator defined before. So at $t=\infty$ it will be in $\ket{0}$, it may have reached this state emitting a single photon (probability $|\beta_0|^2$) or without emitting a photon (probability $|\alpha_0|^2$).
    In the MCWF formalism
    \begin{align}
        \delta p &= \Gamma |\beta_0|^2 \delta t\\
        \ket{\phi^{(1)}(\delta t)} &= \alpha_0 \ket{0} + \beta_0 \exp(-i\omega_0 \delta t) \exp(-\Gamma \delta t/2)\ket{1}\\
        \epsilon &< \delta p \rightarrow \text{quantum jump} \rightarrow \ket{\phi(t+\delta t)} = \ket{0}\\\nonumber
        \epsilon &> \delta p \rightarrow \ket{\phi(t+\delta t)}= \alpha_0\left(1+\frac{\Gamma\delta t}{2}|\beta_0|^2\right)\ket{0}\\&+\beta_0\left(1-\frac{\Gamma \delta t}{2}|\alpha_0|^2\right)\exp(-i\omega_0\delta t)\ket{1}
    \end{align}
\end{frame}

\begin{frame}
    Assuming that no quantum jump occurs between $0$ and $t$:
    \begin{align}
        \dot{\alpha} &= \Gamma \alpha |\beta|^2/2\\
        \dot{\beta} &= \Gamma \beta |\alpha|^2/2
    \end{align}
    \begin{align}
        \label{motion}
        \alpha(t) &= \alpha_0 \left[|\alpha_0|^2+|\beta_0|^2\exp(-\Gamma t)\right]^{-1/2}\\
        \beta(t) &= \beta_0 \exp(-\Gamma t/2)\left[|\alpha_0|^2+|\beta_0|^2\exp(-\Gamma t)\right]^{-1/2}\\
        P(t) &= |\alpha_0|^2 + |\beta_0|^2 \exp(-\Gamma t)
    \end{align}
    \begin{align}
        &\text{With probability } P(t) \hspace{1.2 cm}\phi(t) = \alpha(t)\ket{0} + \beta(t)\exp(-i\omega_0 t)\ket{1}\\
        &\text{With probability } 1-P(t) \hspace{0.5 cm}\phi(t) = 0
    \end{align}
\end{frame}

\begin{frame}
    In the general case, suppose that we know that between $0$ and $t$ no quantum jump has occurred. One can show that the evolution given by $\ref{evolution}$ and $\ref{normalization}$ is equivalent to
    \begin{align}
        i\hbar\frac{d}{dt}\ket{\phi} = \left(H+\braket{\phi |\frac{H^\dagger- H}{2}| \phi}\right)\ket{\phi}
    \end{align}
    For a time indipendent Hamiltonian, the solution is
    \begin{align}
        \ket{\phi(t)} = \frac{1}{\sqrt{\braket{\phi(0)|\exp(iH^\dagger t/\hbar)\exp(-iHt/\hbar|\phi(0))}}}\exp(-iHt/\hbar)\ket{\phi(0)}
    \end{align}
\end{frame}


\begin{frame}
    \section{Correlation Function: ME and MCWF approaches}
    \frametitle{Master-Equation Approach to Correlation Function}
    Suppose we want to compute
    \begin{align}
        \label{corr}
        C(t, \tau) = \braket{A(t+\tau)B(t)}
    \end{align}
    Expand $A= \sum_{ij}A_{ij}X_{ij}$ where $X_{ij} = \ket{i}\bra{j}$ and calculate the value
    \begin{align}
        C(t, \tau) = \braket{X_{ij}(t+\tau)B(t)}
    \end{align}
    In the master-equation approach
    \begin{align}
        \frac{d}{dt}\braket{X_{ij}(t)} = \braket{j|\frac{d}{dt}\rho(t)|i} = \sum_{kl}\mathcal{L}_{ijkl}\braket{X_{kl}(t)}
    \end{align}
    where the coefficients $\mathcal{L}_{ijkl}$ are accordingly known from the master-equation. Thus we have the \textit{quantum regression theorem}:
    \begin{align}
        \frac{\partial}{\partial \tau}C_{ij}(t, \tau) = \sum_{kl}\mathcal{L}_{ijkl}C_{kl}(t, \tau)
    \end{align}
\end{frame}

\begin{frame}
    \frametitle{Monte Carlo Approach}
    We first let $\ket{\phi(0)}$ evolve from $0$ to $t$. For a given outcome of this evolution we form the four 
    new states
    \begin{align}
        \ket{\chi_{\pm}(0)} = \frac{1}{\sqrt{\mu_{\pm}}}(1\pm B)\ket{\phi(t)}\\
        \ket{\chi'_{\pm}(0)} = \frac{1}{\sqrt{\mu'_{\pm}}}(1\pm iB)\ket{\phi(t)}
    \end{align}
    where $\mu_{\pm}, \mu_{\pm}'$ are the normalization coefficients. Now evolve $\ket{\chi_{\pm}(\tau)}, \ket{\chi'_{\pm}(\tau)}$ and then calculate
    \begin{align}
        c_{\pm}(\tau) = \braket{\chi_\pm(\tau)|A|\chi_{\pm}(\tau)}\\
        c'_{\pm}(\tau) = \braket{\chi'_\pm(\tau)|A|\chi'_{\pm}(\tau)}
    \end{align}
    and we obtain the correlation function \ref{corr}
    \begin{align}
        C(t, \tau) = \frac{1}{4}[\mu_+\overline{c_+(\tau)} - \mu_-\overline{c_-(\tau)} -i\mu_+'\overline{c_+'(\tau)} + i\mu_-'\overline{c_-'(\tau)}]
    \end{align}
\end{frame}

\begin{frame}
    We need a prescription for the $\overline{c(\tau)}$ averages:
    \begin{align}
    \ket{\phi(0)}\xrightarrow[]{\text{outer loop}}\ket{\chi(0)}\xrightarrow[]{\text{inner loop}}\ket{\chi(\tau)}
    \end{align}
    \textbf{Proof.}
        Consider
        \begin{align}
        \nonumber
        \kappa_{ij} = \frac{1}{4}[\mu_+\braket{\chi_+(\tau)|X_{ij}|\chi_+(\tau)}-\mu_-\braket{\chi_-(\tau)|X_{ij}|\chi_-(\tau)}\\
        -i\mu'_+\braket{\chi'_+(\tau)|X_{ij}|\chi_+'(\tau)}+i\mu_-'\braket{\chi_-'(\tau)|X_{ij}|\chi_-'(\tau)}]
        \end{align}
        We will prove that $\overline{\kappa_{ij}} =C_{ij}(t, \tau)$. 
        Just show that
        \begin{align}
            \kappa_{ij}(0) = \braket{\phi(t)|X_{ij}B|\phi(t)}
        \end{align}
        so that
        \begin{align}
            \label{initial}
            \overline{\kappa_{ij}}(0) = C_{ij}(t, 0)
        \end{align}
\end{frame}

\begin{frame}
        Suppose we consider fixed initial conditions $\chi_{\pm}(0)$ and $\chi'_{\pm}(0)$. Now, averaging in the inner loop, it's clear that
        \begin{align}
            \frac{\partial}{\partial \tau}\overline{\kappa_{ij}}(\tau) = \sum_{ij}\mathcal{L}_{ijkl}\overline{\kappa_{kl}}(\tau)
        \end{align}
        so the evolution of $\overline{\kappa_{ij}}(\tau)$ and $C_{ij}(t, \tau)$ are the same.
        The average on the outer loop gives us equality \ref{initial}.
\end{frame}

\begin{frame}
    \frametitle{Example: Position of the damped harmonic oscillator}
    Consider the symmetric position correlation function of the damped harmonic oscillator
    \begin{align}
        C_{s}(t, \tau)&=\braket{X(t+\tau)X(t)+X(t)X(t+\tau)}\\
        X &= (b+b^\dagger)/\sqrt{2}
    \end{align}

    \textbf{Trick.} For a Hermitian operator $B$, the symmetric and antisymmetric correlation functions $\braket{A(t+\tau)B(t)\pm B(t)A(t+\tau)}$ can be determined from mean values with only a pair of functions $\ket{\chi_{\pm}(\tau)}$ or $\ket{\chi_{\pm}'(\tau)}$.

    Start with $\ket{\phi(0)} = \ket{0}$ which is a steady state. So the Monte Carlo evolution of $\ket{\phi}$ between $0$ and $t$ is trivial. At time $t$ we construct the two new wave functions $\ket{\chi_{\pm}(0)}$ which in this simple example give
    \begin{align}
        \ket{\chi_\pm(0)} = \sqrt{2/3}\ket{0} \pm 1/\sqrt{3}\ket{1}
    \end{align}
    \end{frame}
    \begin{frame}
    From equation $\ref{motion}$ we get
    \begin{align}
        \nonumber
        c_+(\tau) = -c_{-}(\tau) = P(\tau)(1/\sqrt{2})[\alpha^*(\tau)\beta(\tau)\exp(-i\omega_0\tau)\\+\alpha(\tau)\beta^*(\tau)\exp{(i\omega_0\tau)}]
    \end{align}
    which gives
    \begin{align}
        C_s(t, \tau) = \cos(\omega_0\tau)\exp(-\Gamma\tau/2)
    \end{align}
\end{frame}

\begin{frame}
    \frametitle{Example: Emission by a laser-driven two level atom}
    We suppose here that the laser is strictly resonant so that the atom-laser coupling can be written in the rotating wave approximation     
    \begin{align}
        H_0 =(\hbar\Omega/2)(S^++S^-)
    \end{align}
    From \textit{Loudon}, we know that if we work in steady state the fluorescence spectrum is given by the Fourier transform of the normalized correlation function $C(t, \tau) = \braket{S^+(t+\tau) S^-(t)}$:
    \begin{align}
        F(\omega_{sc})=\frac{1}{2\pi}\int_{\infty}^{-\infty}d\tau \frac{C(\tau)}{C(0)}\exp{(i\omega_{sc}\tau)}
    \end{align}
    One can calculate the spectrum using the optical Bloch equation + quantum regression theorem starting from the steady state.
\end{frame}

\begin{frame}
    And for a MCWF? We start at $t = 0$ in the ground state and we let the MCWF evolve for a time sufficiently long to have several quantum jumps (spontaneous emission). In a density-matrix description this guarantees that the steady state has been reached. In the MCWF approach it implies that there is no memory of initial conditions. In the figure $n_1$ is the number of inner loops and $n_2$ is the number of outer loops. As observed, the higher values of $n_2$ lead to the best results because they provide a closer description of the state at time $t$.
\end{frame}

\begin{frame}
    \begin{tabular}{C{5cm}  L{5cm}}
        \includegraphics[height=\textheight]{figures/laser.png} &
Solid curves: real part (upper curve) and imaginary part of the dipole correlation function for a two-level atom $\braket{S^+(t+\tau)S^-(t)}/\braket{S^+(t)S^-(t)}$, for various choices of the numbers $n_1$ and $n_2$. Dotted curves: we have indicated the exact result obtained by using optical Bloch equations and the quantum regression theorem. The field parameters of the calculations are $\Omega = 10 \Gamma$, $\delta = 0$.
    \end{tabular}
\end{frame}


\begin{frame}
    \frametitle{Example: Spontaneous Emission with Zeeman Degeneracy}
    \section{Example: Spontaneous Emission with Zeeman Degeneracy}
    We go back to the problem of spontaneous emission of a two-level system, but this time we take into account the angular momentum of the ground ($J_g$) and excited ($J_e$) levels. We choose a quantization axis $z$:
    \begin{align}
        \label{zeemanrelax}
        \mathcal{L}(\rho_S) = -\Gamma/2 (P_e\rho_S+\rho_S P_e)+\Gamma\sum_q S^-_q\rho_S S^+_q
    \end{align}
    where $q$ is the standard basis associated with the $z$ axis
    \begin{align}
        S^+_1=-\left[\frac{S^+_x+iS^+_y}{\sqrt{2}}\right],
        S^+_{-1}=+\left[\frac{S^+_x-iS^+_y}{\sqrt{2}}\right],
        S^+_0 = S_z^+
    \end{align}
    where $(S_q^+)^\dagger = S_q^-$ and the operator $P_e$ is the ground state projector. Using the fact that $\vec{S}^-$ and $\vec{S}^+$  are vector operators and applying the Wigner-Eckart theorem:
\end{frame}

\begin{frame}
    \begin{align}
        \braket{j, m | S_q^+ | j', m'} = (1, j', q, m'; j, m) \braket{j || S_q^+ || j'}
    \end{align}
But we want $S_q^+$ to be a raising operator and the ground state and the excited state have definite total angular momentum, so the reduced matrix element is just $\braket{j || S_q^+ || j'} = \delta_{j, J_e}\delta_{j', J_g}$. Thus we have
\begin{align}
    S_q^+\ket{J_g, m_g} &= (1, J_g, q, m_g; J_e, m_e=m_g+q) \ket{J_e, m_e=m_g+q}\\
    S_q^+\ket{J_e, m_e} &= 0
\end{align}
more explicitly $S_q^+$ will be of the form
\begin{align}
    S_q^+ = P_g V_q P_e
\end{align}
where $V_q$ is generic vector operator. Notice that $S_q$ will still be a vector operator because rotation operators $T$ don't mix different representations.
\end{frame}

\begin{frame}
    Now notice that $\sum_q S^+_q S^-_q$ is a scalar and is non zero only on the ground state subspace. So we have
    \begin{align}
        \sum_q S^+_q S^-_q = P_e
    \end{align}
    This ensure that the relaxation operator \ref{zeemanrelax} is of the standard Lindblad form if we take $C_q = \Gamma ^{1/2} S_q^-$.

    From the measurement point of view presented before, this corresponds to a simulation in which not only the number of photons emitted during the $\delta t$ is detected but also the angular momentum of those photons along a given axis $z$.
\end{frame}

\begin{frame}
    \frametitle{Equivalent Monte Carlo simulations for a given master-equation}
    \section{Equivalent Monte Carlo simulations for a given master-equation}
    We discuss the existence of several different Monte Carlo approaches for $\mathcal{L}$, leading in average to the same results, but with possibly different physical pictures.\\
    Consider for example the relaxation operator introduced before in Eq. \ref{zeemanrelax}:
    \begin{align}
        \nonumber
        \mathcal{L}(\rho_S) = -\Gamma/2 (P_e\rho_S+\rho_S P_e)
        \\ + S_x^-\rho_S S_x^++S_y^-\rho_S S_y^++S_z^-\rho_S S_z^+
    \end{align}
    Our dissipation process is isotropic. Thanks to the vector transform properties, the dissipation term is a scalar (in fact it is a scalar product), so that if $T$ is for istance a rotation operator we have that:
    \begin{align}
        T \mathcal{L}(\rho_S) T^\dagger = \mathcal{L}(T\rho_S T^\dagger)
    \end{align}
\end{frame}

\begin{frame}
    This means we can write the relaxation operator alternatively with new operators $D_m$:
    \begin{align}
       D_m = T^\dagger C_m T 
    \end{align}
We can therefore perform the Monte Carlo simulation either with the set of operators $C_m$ or with the set of operators $D_m$. The physical pictures given by these two Monte Carlo simulation may be quite different from each other, although we know that their predictions concerning one time averages or two-time correlation functions are the same.
\end{frame}

\begin{frame}
    \frametitle{Example}
    Consider a $g, J_g = 1 \leftrightarrow e, J_e = 1$ transition irradiated by two resonant laser fields with the same intensity and polarized $\sigma_+$ and $\sigma_-$ with the respect to the $z$ axis. The free hamiltonian is
    \begin{align}
        H_0 = i\hbar \Omega /2 \left[\left(S_1^+ + S_1^-\right) + \left(S_{-1}^+ + S_{-1}^-\right)\right]
    \end{align}
    that can be rewritten as
    \begin{align}
        H_0 = \sqrt{2}\hbar \Omega \left[S_y^+ + S_y^-\right]
    \end{align}
    In fact the resulting field polarization is linear and parallel to $y$. It is known from the analysis by optical Bloch equations that the atomic population is eventually trapped in a ground state that is not coupled to the laser field (see \textit{Scully} for a detailed derivation of this dark state phenomenon). If the two waves are in phase, this dark state is:
    \begin{align}
        \ket{\phi_{NC}} = \frac{\ket{g, m_z = -1}+\ket{g, m_z = 1}}{\sqrt{2}}
    \end{align}
\end{frame}

\begin{frame}
    \begin{figure}[H]
        \includegraphics[width=0.6\textwidth]{figures/scheme.png}
        \caption{\tiny Configuration schemes leading to a dark resonance: a $g, J_g = 1 \leftrightarrow e, J_e = 1$ transition is irradiated by two waves respectively $\sigma_+$ and $\sigma_-$ polarized along the z axis. \textbf{a} If angular momentum is quantized along the z axis, the dark resonance appears as the formation of a nonabsorbing state, which is a linear combination of $\ket{g, m_z = -1}$ and $\ket{g, m_z = 1}$. \textbf{b} If angular momentum is quantized along the axis parallel with the resulting linear polarization of the light (y axis), the dark resonance corresponds to an optical pumping to the $\ket{g, m_y = 0}$, which is not coupled to the light because the Clebsch-Gordan coefficient connecting $\ket{g, J_g = 1, m_y = 0}$ and $\ket{e, J_e = 1, m_y = 0}$ is zero.}
    \end{figure}
\end{frame}

\begin{frame}
    \textbf{First MCWF analysis.} Let's make use of the operator $C_q$ defined using $z$ as quantization axis. 
    \begin{itemize}
        \item Suppose that the atom starts for instance in $\ket{g, m_z = -1}$.
        \item The atom-laser coupling $H_0$ leads first to an increase of the population of the excited state $\ket{e, m_z=0}$.
        \item A spontaneous photon may then be emitted which, depending on its angular momentum $q_z = \pm 1$, puts the atom back into $\ket{g, m_z = \mp 1}$. The transition $q_z = 0$ cannot be detected, because the only excited state we can populate is $\ket{e, m_z = 0}$ and the transition $\ket{e, m_z = 0} \rightarrow \ket{g, m_z = 0}$ is forbidden because of the vanishing Clebsch-Gordan coefficient.
        \item However, it may also happen that no spontaneous photon is detected after a long time, with the successive steps of evolution owing to the non Hermitian $H$ hamiltonian, causing a continuous rotation from $\ket{g, m_z = \pm 1}$ into $\ket{\phi_{NC}}$ and therefore trapping the atomic population into this state.
    \end{itemize} 
\end{frame}

\begin{frame}
    \textbf{Second MCWF analysis.} We replace the set of operators $C_q$ by a different set choosing the $y$ quantization axis. We now identify the trapping state as $\ket{\phi_{NC}} = \ket{g, m_y = 0}$.
    \begin{itemize}
        \item We start again in the state $\ket{g, m_z = -1}$ that we expand on the $\ket{g, m_y}$ basis. In the first part of the evolution a continuous rotation toward $\ket{g, m_y = 0}$ take place.
        \item Then a first photon with $q_y = 0$ is detected. This detection projects the wave function onto a superposition of $\ket{g, m_y = \pm 1}$ and the population of the trapping state $\ket{g, m_y = 0}$ is zero.
        \item The atom then cycles between $\ket{g, m_y = \pm 1}$ and $\ket{e, m_y = \pm 1}$ and the population of the trapping state remains zero.
        \item Until one detects a second photon with polarization $q_y = \pm 1$. This detection projects the wave function into the trapping state.
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{figure}[H]
        \includegraphics[width=0.8\textwidth]{figures/dark_resonance.png}
        \caption{\tiny MCWF simulation of a dark resonance. The population of the uncoupled state in a single MCWF evolution, corresponding to a measurement of the photon angular momentum along the z axis, \textbf{a}, or along the y axis, \textbf{b}. The two types of evolution are clearly different. Average of 100 MCWF evolutions, with a measurement of the photon angular momentum along the z axis, \textbf{c}, or along the y axis, \textbf{d}. Apart from fluctuations, the two simulations lead to the same result, as expected.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Getting good statistics with the MCWF method}
    \section{Getting good statistics with the MCWF method}
    \textbf{Definition of a Signal-to-Noise ratio}
    
    Goal: determine the average value $\braket{A}(t)$ at time $t$ of a given Hermitian system operator $A$, knowing the state of the system at $t = 0$. Running the MCWF method with a number $n$ of simulations we obtain the sample mean
    \begin{align}
        \braket{A}_{(n)}(t) = \frac{1}{n}\sum_{i=1}^n \braket{\phi^{(i)}(t)|A|\phi^{(i)}(t)}
    \end{align}
    which will approximate $\braket{A}(t)$ with a statistical error $\delta A_{(n)}$ related to the square root of the sample variance $(\Delta A_{n})^2$ by
    \begin{align}
        \delta A_{(n)} = \Delta A_{(n)}/\sqrt{n}
    \end{align}
    where
    \begin{align}
        (\Delta A)^2_{(n)}(t) = \frac{1}{n}\left(\sum_{i = 1}^n \braket{\phi^{(i)}(t)|A|\phi^{(i)}(t)}^2\right)-\braket{A}_{(n)}^2(t)
    \end{align}
\end{frame}

\begin{frame}
    When $n \gg 1$, the sample variance tends to a finite value that we denote $(\Delta A)^2_{\infty}$. Consequently, the condition for having a good signal-to-noise ratio $\braket{A}/\delta A_{(n)}$ can be written as
    \begin{align}
        \sqrt{n} \gg \Delta A_{(\infty)}(t)/\braket{A}(t)
    \end{align}
    Let's give an estimate of the sample variance for $n = \infty$ an infinite number of iterations $(\Delta A)^2_{\infty}(t)$.
    We then discuss the requirement on $n$ given by the inequality in terms of \textit{local} and \textit{global} operators.
\end{frame}

\begin{frame}
    \textbf{Extimate of the Sample Variance}

    The trick to estimate $(\Delta A)^2_{\infty}(t)$ is to use the inequality:
    \begin{align}
        \braket{\phi|A|\phi}^2 \leq \braket{\phi|A^2|\phi}
    \end{align}
    Notice that the inequality is saturated if $\ket{\phi}$ is an eigenstate of $A$. Thus
    \begin{align}
        (\Delta A)^2_{(n)}(t) \leq \frac{1}{n}\left(\sum_{i = 1}^n \braket{\phi^{(i)}(t)|A^2|\phi^{(i)}(t)}\right)-\braket{A}_{(n)}^2(t)
    \end{align}
    When $n \gg 1$ the two terms of this expression have a finite limit: $\braket{A^2}(t)$ and $\braket{A}^2(t)$ where the averages are now taken with the system density matrix at time $t$. The right hand side of inequality therefore tends to the variance $(\Delta A)^2_{\rho_S}(t)$ of the operator $A$ for  a system in a state described by the density matrix $\rho_S(t)$:
    \begin{align}
        (\Delta A)^2_{\rho_S}(t) = \text{Tr}\left[\rho_S(t)A^2\right]-\text{Tr}\left[\rho_S(t)A\right]^2
    \end{align}
    we therefore have the following overestimate of $(\Delta A)^2_{\infty}(t)$:
    \begin{align}
    (\Delta A)^2_{\infty}(t) \leq (\Delta A)^2_{\rho_S}(t)
    \end{align}
\end{frame}

\begin{frame}
    Consequently a sufficient condition for $n$ is given by
    \begin{align}
        \label{keyineq}
        \sqrt{n} \gg \Delta A_{\rho_S}(t)/[\braket{A}(t)]
    \end{align}
    Let us also emphasize that $\Delta A_{(\infty)}(t)$ can vary for a given system with the choice of the set of $C_m$ operators: it is not a function of observable averages or correlations!

    Choose for example $A = \ket{\phi_{NC}}\bra{\phi_{NC}}$, whose average value gives the population of the trapping state. We plot the value of $\Delta A_{(\rho_S)}(t)$ and of the $\Delta A_{(n)}(t)$ values obtained for the two simulations with the $y$ and the $z$ quantization axes. The initial state is $\ket{g, m_z = -1}$ for the three curves. We have taken $n = 1000$ so that $\Delta A_{(n)}(t) \approx \Delta A_{(\infty)}(t)$.
\end{frame}

\begin{frame}
    \begin{figure}[H]
        \includegraphics[width=0.8\textwidth]{figures/convergence.png}
        \caption{Time evolution of the variance $\Delta A_{(\rho_S)}(t)$ and of the sample variances $\Delta A_{(n)}(t)$ obtained for the two simulations with the y and z quantization axis for the dark resonance problem $(n = 1000)$. The sample variance for the y axis choice is close to its upper bound $\Delta A_{(\rho_S)}(t)$, whereas the simulation with the choice of the z axis leads to a noise that is more than two times smaller.}
    \end{figure}
\end{frame}

\begin{frame}
    We also note that the simulation with the y quantization axis has $\Delta A_{(\infty)}$ close to its upper bound; this is easily understood since in this case, as soon as one quantum jump occurs, the wave function is in an eigenstate of $A$, with the eigenvalue $0$ if the detected photon has polarization $q_y = 0$ or $1$ if it has $q_y = \pm 1$.
    
    The simulation with quantization along the $z$ axis leads to $\Delta A_{(n)}(t)$ smaller by a factor of $2$ and is therefore more appropriate if one looks for a rapidly converging procedure.
\end{frame}

\begin{frame}
    \textbf{Local versus Global operators}

    We now split the various operators $A$ in two kinds. First, there are \textit{local operators}, such as the ones giving the population of a particular state $\ket{j}$, $A = \ket{j}\bra{j}$ and $A^2 = A$.
    Consider a simulation that involves $N$ quantum levels. For simplicity, suppose that after some time we have a density matrix that is evenly populated:
    \begin{align}
        \rho(t) =
        \begin{bmatrix}
            1/N & & &\\
            & \cdots & & \\
            & & \cdots & \\
            & & & 1/N
        \end{bmatrix}
    \end{align}
    For those operators we expect:
    \begin{align}
        \braket{A}(t) &= [\rho_S(t)]_{jj} \approx 1/N \\
        (\Delta A)^2_{\rho_S}(t) &= \text{Tr}\left[\rho_S(t)A^2\right]-\text{Tr}\left[\rho_S(t)A\right]^2 \approx 1/N - (1/N)^2 \approx 1/N
    \end{align}
    If we insert these values into inequality \ref{keyineq}, we see that
    \begin{align}
        \text{local operators}\hspace{1cm} n \gg N
    \end{align}
\end{frame}

\begin{frame}
    Clearly one does not gain by using a Monte Carlo treatment in this case. The amount of calculation for determining a single $\ket{\phi(t)}$ is reduced by a factor of $N$ with respect to the calculation of the density matrix $\rho_S(t)$, but one has to repeat $n$ MCWF runs to get good statistics, with $n > N$.
\end{frame}

\begin{frame}
    The MCWF treatment is more efficient if one deals with global operators, such as the population of a large group or the average kinetic energy. For those operators we have
    \begin{align}
        \Delta A_{(\rho_S)}(t) \approx \braket{A}(t)
    \end{align}
    For example, the problem of a Brownian particle thermalized with a reservoir at temperature $T$ with $A = P^2/2M$ gives
    \begin{align}
        \braket{A} = \frac{3}{2}k_BT\\
        \Delta A_{(\rho_S)} = \sqrt{\frac{3}{2}}k_BT
    \end{align}
    In this case inequality $\ref{keyineq}$ tells us that good statistics are obtained after $n$ Monte Carlo runs as soon as
    \begin{align}
        \text{global operators}\hspace{1cm} n \gg 1
    \end{align}
\end{frame}

\begin{frame}
    Recall that $\braket{A}$ is affected by a statistical error of $\delta A = \Delta A/\sqrt{n}$. So if we want a $10\%$ accuracy, we have \textbf{for global operators}
    \begin{align}
        10\% = \frac{\delta A}{\braket{A}} = \frac{\Delta A}{\braket{A} \sqrt{n}} = \frac{1}{\sqrt{n}}
    \end{align}
    So we need $n \approx 100$ runs. Thus when the number $N$ of levels involved is larger than this number, the MCWF may be more efficient than the master-equation approach.
\end{frame}

\begin{frame}
    \frametitle{Conclusions}
    \section{Conclusions}
    \begin{itemize}
        \item We have presented a MC stochastic evolution for the wave function of a system that can be described in a master-equation frame.
        \item We have proved the equivalence of this MCWF method with the master-equation treatment and we have given several examples where it can be applied.
        \item We have considered in detail the efficiency with which expectation values of physical observables may be calculated with this method.
        \item In order to control the fluctuations inherent in any simulation procedure, one must propagate a large number of wave functions and, depending on the dimensionality of the system Hilbert space and on the type of observable considered, the simulations may be more or less efficient than the master-equation treatment.
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Apart from the numerical procedure, the replacement of the density matrix by wave functions provides new insight; for example, it reveals mechanisms for the evolution of the system that may manifest themselves less clearly in the master-equation approach.
        \item As seen from another perspective, this work confronts two very different definitons of the density matrix:
        \begin{enumerate}
            \item a reduction, by mean of a trace, of the state of the combined system $+$ reservoir, which can no longer be described by a pure state in the small system
            \item a statistical description of an ensemble of systems populating different states with a given propability law.
        \end{enumerate}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Which of the two underlying interpretations for the density matrix is used has no influence on the way in which mean values are obtained: the master-equation, which is derived from the first definition for the density matrix, can be treated by our MCWF method, which relies on the second interpretation.
    \end{itemize}
\end{frame}

\begin{frame}{Additional content}
    When the ground state and the excited levels have no degeneracy, we don't care about the vectorial properties of the dipole operator. We take a dipole interaction:
    \begin{align}
        H_{ED} = e\vec{D}\cdot \vec{E}_0 \cos(\omega t)
    \end{align}
    where the total dipole moment of the atom is written as
    \begin{align}
        \hat{\vec{D}} = \sum_{\alpha = 1}^Z \hat{\vec{r}}_\alpha
    \end{align}
    We have that $H_{ED}$ changes sign under inversion in the nucleus! If we assume that the ground state and the excited state are odd or even function of the electron positions, we have that
    \begin{align}
        \braket{g|H_{ED}|g} = 0\\
        \braket{e|H_{ED}|e} = 0
    \end{align}
\end{frame}
\begin{frame}
    Thus
    \begin{align}
        H_{ED} = e\vec{D}\cdot \vec{E_0} \cos(\omega t) \ket{g}\bra{e} \text{+ h.c.}
    \end{align}
    where $\vec{D} = \braket{g|\hat{\vec{D}}|e}$. So for example if $\vec{E}_0 = E_0 \hat{x}$ we have
    \begin{align}
        H_ED &= e \braket{g|\hat{x}|e}E_0 \cos(\omega t) \ket{g}\bra{e} \text{+h.c.}\\
        &= \hbar \Omega \ket{g}\bra{e} \text{+h.c.}
    \end{align}

    In the case of Zeeman degeneracy we just use the fact that $\hat{\vec{D}}$ is a vector operator.
\end{frame}
\end{document}